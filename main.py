import asyncio
import os

import nest_asyncio
import spotipy
from spotipy.oauth2 import SpotifyOAuth
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Message, Update
from telegram.ext import (
    Application,
    CallbackQueryHandler,
    CommandHandler,
    ContextTypes,
    MessageHandler,
)

TELEGRAM_TOKEN = os.getenv("TELEGRAM_TOKEN")
KIND_HOME_CHAT_ID = -1001692721601
MUSIC_TOPIC_ID = 489


async def spotify(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    query = update.message.text
    tracks = sp.search(query)["tracks"]["items"]
    keyboard = [
        [InlineKeyboardButton(format_track(track), callback_data=track["uri"])]
        for track in tracks
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    await update.message.reply_text("Wybierz utwór:", reply_markup=reply_markup)


async def button(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    query = update.callback_query
    await query.answer()
    if query.data == "unpause":
        sp.start_playback()
        await refresh_player()
    elif query.data == "pause":
        sp.pause_playback()
        await refresh_player()
    elif query.data == "next":
        sp.next_track()
        await refresh_player()
    else:
        uri = query.data
        sp.add_to_queue(uri)
        await query.message.edit_text(
            f"Dodano {format_track(sp.track(uri))}", reply_markup=None
        )
        await renew_player(update, context)


async def queue(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    current_queue = sp.queue()
    await update.message.reply_text(current_queue)


def format_track(track) -> str:
    return (
        f'{track["name"]} - {" & ".join(artist["name"] for artist in track["artists"])}'
    )


def get_playback_data():
    track = sp.currently_playing()
    if track:
        return (
            format_track(track["item"]) if track["item"] else "Nie ma nic w kolejce",
            track["is_playing"],
        )
    else:
        return "Nie ma nic w kolejce", False


def prepare_message_text_and_keyboard() -> (str, InlineKeyboardMarkup):
    message_text, is_playing = get_playback_data()
    inline_keyboard = InlineKeyboardMarkup(
        [
            [
                InlineKeyboardButton("⏸", callback_data="pause")
                if is_playing
                else InlineKeyboardButton("⏵", callback_data="unpause"),
                InlineKeyboardButton("⏭", callback_data="next"),
            ]
        ]
    )
    return message_text, inline_keyboard


async def initialize() -> Message:
    message_text, inline_keyboard = prepare_message_text_and_keyboard()
    message = await application.bot.send_message(
        KIND_HOME_CHAT_ID,
        message_text,
        message_thread_id=MUSIC_TOPIC_ID,
        reply_markup=inline_keyboard,
    )
    return message


async def renew_player(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    global playback_message
    new_playback_message = await initialize()
    await playback_message.delete()
    playback_message = new_playback_message


async def refresh_player() -> None:
    global playback_message
    new_text, inline_keyboard = prepare_message_text_and_keyboard()
    if (
        new_text != playback_message.text
        or inline_keyboard != playback_message.reply_markup
    ):
        playback_message = await playback_message.edit_text(
            new_text, reply_markup=inline_keyboard
        )


async def main() -> None:
    global playback_message
    playback_message = await initialize()
    t = asyncio.create_task(player_message_loop())
    application.run_polling()
    await playback_message.delete()
    t.cancel()


async def player_message_loop() -> None:
    while True:
        await asyncio.sleep(5)
        await refresh_player()


if __name__ == "__main__":
    nest_asyncio.apply()
    playback_message: Message = None
    scope = "user-modify-playback-state user-read-currently-playing"
    sp = spotipy.Spotify(auth_manager=SpotifyOAuth(scope=scope))
    application = Application.builder().token(TELEGRAM_TOKEN).build()
    application.add_handler(CommandHandler("play", spotify))
    # application.add_handler(CommandHandler('queue', queue))
    application.add_handler(CallbackQueryHandler(button))
    application.add_handler(MessageHandler(None, renew_player))
    asyncio.run(main())
